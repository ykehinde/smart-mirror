# Smart Mirror

1000heads Smart Mirror built with node for the Raspberry Pi.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
npm install
```

### Run the server and watch server files
#### Nodemon

`npm start` will run the following
```
nomdemon app.js localhost 3000 && grunt watcher
```

`npm pm2` will run the following
```
pm2 start app.js && grunt watcher
```

### Compile, then watch files
Run `grunt watcher` to watch the files (if you haven't used `npm start`)

## Front-end source files
Front-end source files can be found in the `./assets/src` directory. These compile to the `./public/*` directory.

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Use `npm run deploy` to compile & compress static assets in CI environments.

## Built With

* [Express Generator](https://expressjs.com/en/starter/generator.html) - Fast, unopinionated, minimalist web framework for Node.js
<!-- * [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds -->

<!-- ## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us. -->

## Versioning

We use Bitbucket for versioning. For the versions available, see the [repo](https://bitbucket.org/1000heads/smart-mirror/). 

## Authors

* **Yemi Kehinde** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)
* **Daniel Murdolo** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
