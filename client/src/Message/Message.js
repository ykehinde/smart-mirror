import React, { Component } from 'react';
import messages from "../config/messages";
import "./Message.css";
import axios from 'axios';

class Messages extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      message: ''
    }
    this.getMessage = this.getMessage.bind(this);
    this.callApi = this.callApi.bind(this);
  }
  
  componentWillMount() {
    this.callApi();
  }

   componentDidMount() {
     setInterval(this.callApi, 60000);
   }

  callApi() {
    let url = 'http://1000heads.local:3100/api/messages/list';

    axios.get(url)
      .then((data) => {
        this.getMessage(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getMessage(data) {
    let message = data.data.messages;
    let currentMessage;

    for (let i = 0; i < message.length; i++) {
      const element = message[i];
      
      if (element.state === true) {
        currentMessage = element.message;
      }
    }
    
    
    this.setState({
      message: currentMessage
    })
  }

  render() {
    return ( 
      <div className="qotd">
        <h2 className="quote">{this.state.message}</h2>
      </div>
    );
  }
}

export default Messages;