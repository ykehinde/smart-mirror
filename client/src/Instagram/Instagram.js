import React, { Component } from 'react';
import axios from "axios";
import TinySlider from "tiny-slider-react";
import './Instagram.css';

const settings = {
  loop: true,
  autoplay: true,
  mode: 'gallery',
  nav: false,
  autoHeight: true,
  autoWidth: true,
  controls: false,
  speed: 400,
  animateDelay: 8000,
  freezable: false,
  autoplayButton: false,
  autoplayHoverPause: false,
  container: "#instagram"
};

class Instagram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: []
    }    
  }
  
  componentWillMount() {
    const url = `http://localhost:3159/instagram`;

    axios.get(url)
      .then((response) => {
        this.setState({
          posts: response.data.data
        })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div id="instagram" className="instagram">
      <TinySlider settings={settings}>
          {this.state.posts.map((post,i) =>
            
            <div key={post.id} className="post-container">
              <div className="instagram-post">
            <img className="" src={post.images.standard_resolution.url} alt=""/>
              </div>
            </div>
          )}
        </TinySlider>
      </div>
    );
  }
}

export default Instagram;