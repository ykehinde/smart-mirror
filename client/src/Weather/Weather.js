import React, { Component } from 'react';
import axios from "axios";
import params from "../config/parameters";
import Forecast from "../Forecast/Forecast";
import './Weather.css';

class Weather extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mainIcon: '',
      tempMain: '',
      location: '',
      data: {},
      middayWeather: [],
      forecastIcon: ''
    }
  }
  
  componentWillMount() {
     this.makeWeatherCall();
     this.makeForecastCall();
  }

  componentDidMount() {
    this.callEveryHour();
  }
  

  callEveryHour(){
    setInterval((this.makeWeatherCall, this.makeForecastCall), 1000*60*60);
  }

  makeWeatherCall() {
    const url = `${params.OPENWEATHER_URL}${params.OPENWEATHER_WEATHER}?q=${params.OPENWEATHER_CITY}&units=metric&appid=${params.OPENWEATHER_APPID}`;

    axios.get(url)
      .then((response) => {
        this.populateMainWeather(response);
      })
      .catch((error) => {
        console.log(error);
      });      
  }

  makeForecastCall() {
    const url = `${params.OPENWEATHER_URL}${params.OPENWEATHER_FORECAST}?q=${params.OPENWEATHER_CITY}&units=metric&appid=${params.OPENWEATHER_APPID}`;

    axios.get(url)
      .then((data) => {
        this.populateForcast(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  populateMainWeather(data) {
    this.setState({
      mainIcon: this.changeIcon(data.data.weather[0].icon),
      noImage: true,
      tempMain: Math.round(data.data.main.temp),
      location: data.data.name + ', ' + data.data.sys.country
    })
  }

  populateForcast(data) {    
    let forecast = data.data.list;

    this.setState({
      middayWeather: forecast
    })
  }

  changeIcon(icon) {
    switch (icon) {
      case "02n":
        return "02d"
      case "03n":
      case "04n":
      case "04d":
        return "03d"
      case "13d":
      case "13n":
        return "13neu"
      case "50d":
      case "50n":
        return "50neu"
      default:
        return icon
    }
  }

  render() {
    let items = this.state.middayWeather;
    return (
      <div>
        <div className="weather--container">
           <div id="icon--main" className="icon">
           {this.state.mainIcon.length > 0 ?
            <img className={this.state.mainIcon} src={"/img/" + this.state.mainIcon + ".svg"} alt="" /> :
            <img src={"/img/wi-na.svg"} alt="" />
            }
           </div> 
          <div className="weather-content">
            <h2 id="temp--main" className="temp--main">{this.state.tempMain}<sup>&deg;</sup>C</h2>
            <h3 id="location" className="location">{this.state.location}</h3>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="forcast--container weather--container">
          <h2>Forecast</h2>
          <ul id="forcast--list">
            {items.length > 0 ? items.filter(item => item.dt_txt.includes('12:00:00') === true).map((item, i) => {
              return (<Forecast
                  key={i}
                  forecastDate={new Date(item.dt*1000).toString()}
                  forecastIcon={item.weather[0].icon}
                  forecastTemp={item.main.temp}
                ></Forecast>);
              }): () => {
                return (
                  <li>No forecast found</li>
                )
              }
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default Weather;