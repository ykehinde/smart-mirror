import React, { Component } from 'react';
import moment from 'moment';
import './Date.css';

class Date extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentDate: '',
      currentTime: ''
    }
  }

  componentDidMount() {
    this.startDay();
    this.startTime();
  }

  startDay() {
    setInterval(() => {
      this.setState({
        currentDate: moment().format('ddd D/MM/YY')
      })
    }, 1800000)
  }

  startTime() {
    setInterval(() => {
      this.setState({
        currentTime: moment().format('LT')
      })
    }, 500)
  }
    
  render() {
    return (
      <div className="Date">
        <h1 id="currentTime">{this.state.currentTime}</h1>
        <h2 id="currentDate">{this.state.currentDate}</h2>
      </div>
    );
  }
}

export default Date;
