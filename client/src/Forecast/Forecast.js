import React, { Component } from 'react';
import "./Forecast.css";

class Forecast extends Component {
  constructor(props) {
    super(props);

    this.state = {
      forecastIcon: ''
    }
  }

   componentWillMount() {     
    this.setState({
      forecastIcon: this.changeIcon(this.props.forecastIcon),
    })
  }

  changeIcon(icon) {
    switch (icon) {
      case "02n":
        return "02d"
      case "03n":
      case "04n":
      case "04d":
        return "03d"
      case "13d":
      case "13n":
        return "13neu"
      case "50d":
      case "50n":
        return "50neu"
      default:
        return icon
    }
  }

  render() {    
    return (
      <li>
        <div className="day--forecast"><h3>{this.props.forecastDate.substring(0, 3)}</h3></div>
        <div className="icon--forecast">
          {this.state.forecastIcon.length > 0 ?
            <img src={"/img/" + this.state.forecastIcon + ".svg"} alt="" /> :
            <img src={"/img/wi-na.svg"} alt="" />
          }
        </div>
        <div className="temp--forcast">{Math.round(this.props.forecastTemp)}<sup>&deg;</sup>C</div>
      </li>
    );
  }
}

export default Forecast;
