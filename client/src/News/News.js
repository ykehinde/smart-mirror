import React, { Component } from 'react';
import params from "../config/parameters";
import axios from 'axios';
import TinySlider from "tiny-slider-react";
import './News.css';

const settings = {
  axis: 'vertical',
  loop: true,
  autoplay: true,
  nav: false,
  controls: false,
  speed: 1000,
  animateDelay: 20000,
  autoHeight: true,
  freezable: false,
  autoplayButton: false,
  autoplayHoverPause: false
};

class News extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articles: []
    }
  }

  componentWillMount() {
    this.getNews();
  }

  componentDidMount() {
    setInterval(this.getNews, 2000 * 60 * 60);
  }

  getNews() {
    // https://newsapi.org/docs/
    let url = `${params.NEWS_URL}${params.NEWS_SOURCE}&apiKey=${params.NEWS_API}`

    axios.get(url)
      .then((news) => {
        let articles = news.data.articles
        this.setState({
          articles: articles
        })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
     return (
      <div id="news-container" className="news-container">
        <ul id="news-items" className="bxslider">
        <TinySlider settings={settings}>
          {this.state.articles.map((item,i) =>
            <li key={i} className="article--item" ref={el => this.el = el}>
              <h3 className="article--title">
                {item.title}
              </h3>
              <p className="article--description">
                {item.description}
              </p>
              <p className="article--source">
                {item.source.name}
              </p>
            </li>
          )}
        </TinySlider>
        </ul>
      </div>
    );
  }
}

export default News;
