(function () {
  function startDay() {
    document.getElementById('currentDate').innerHTML = moment().format('dddd Do MMMM YYYY');
    setInterval(startDay, 3600000);
  }

  function startTime() {
    document.getElementById('currentTime').innerHTML = moment().format('LTS');
    setInterval(startTime, 500);
  }

  function makeWeatherCall() {
    let city = 'London';
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=5d8fc15aa663a4e6e646dad984f51bbd`

    axios.get(url)
      .then(function (data) {
        data = data.data;
        populateMainWeather(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function makeForcastCall() {
    let city = 'London';
    let url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=5d8fc15aa663a4e6e646dad984f51bbd`

    axios.get(url)
      .then(function (data) {
        data = data.data.list;
        populateForcast(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function changeIcon(icon) {
    switch (icon) {
      case "02n":
        return "02d"
      case "03n":
      case "04n":
      case "04d":
        return "03d"
      case "13d":
      case "13n":
        return "13neu"
      case "50d":
      case "50n":
        return "50neu"
      default:
        return icon
    }
  }
  
  function populateMainWeather(data) {
    let weather = {
      _icon: changeIcon(data.weather[0].icon),
      _location: data.name + ', ' + data.sys.country,
      _temp: Math.round(data.main.temp),
    };

    document.getElementById('icon--main').innerHTML = `<img src="/img/${weather._icon}.svg" alt="">`;
    document.getElementById('temp--main').innerHTML = `<h2>${weather._temp}<sup>&deg;</sup>C</h2>`;
    document.getElementById('location').innerHTML = `<h3>${weather._location}</h3>`;
  }


  function populateForcast(forcast) {
    let middayWeather = []
    for (let i = 0; i < forcast.length; i++) {
      const item = forcast[i];

      let contextTime = item.dt_txt;
      
      if(contextTime.includes('12:00:00')) {
        middayWeather.push(item);
      }
    }
    console.log(middayWeather);

    for (let j = 0; j < middayWeather.length; j++) {
      const day = middayWeather[j];
      createDay(day);
    }
  }

  function createDay(day) {
    let _date = new Date(day.dt*1000).toString();
    let _icon = changeIcon(day.weather[0].icon);
    let _temp = Math.round(day.main.temp);

    let forcastList = document.getElementById('forcast--list');

    let _forcastItem = document.createElement('li');
    let _forcastDay = document.createElement('div');
    let _forcastIcon = document.createElement('div');
    let _forcastTemp = document.createElement('div');

    _forcastDay.classList.add('day--forcast');
    _forcastIcon.classList.add('icon--forecast');
    _forcastTemp.classList.add('temp--forcast');

    _forcastDay.innerHTML = `<h3>${_date.substring(0, 3)}</h3>`;
    _forcastIcon.innerHTML = `<img src="/img/${_icon}.svg" alt="">`;
    _forcastTemp.innerHTML = `<p>${_temp}<sup>&deg;</sup>C</p>`;

    forcastList.appendChild(_forcastItem);
    _forcastItem.appendChild(_forcastDay);
    _forcastItem.appendChild(_forcastIcon);
    _forcastItem.appendChild(_forcastTemp);
  }

  function getNews() {
    // https://newsapi.org/docs/
    let source = "bbc-news"
    let NEWS_API = "19f2b39cda364e15b66eb6b3f259cc93"
    let url = `https://newsapi.org/v2/everything?sources=${source}&apiKey=${NEWS_API}`

    axios.get(url)
      .then(function (news) {
        displayNews(news)
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function displayNews(news) {
    let articles = news.data.articles;

    for (let i = 0; i < articles.length; i++) {
      const article = articles[i];
      createNewsItems(article);
    }
    $('#news-items').bxSlider({
      mode: "vertical",
      easing: "ease-in-out",
      pager: true,
      pagerType: 'full',
      controls: false,
      auto: true,
      autoStart: true,
      autoDirection: 'next',
      pagerSelector: "#news-item",
      infiniteLoop: true,
      tickerHover: false,
      adaptiveHeight: true,
      touchEnabled: false
    });
  }

  function createNewsItems(article) {
    let _container = document.getElementById('news-items');

    let _articleItem = document.createElement('li');
    let _articleTitle = document.createElement('h2');
    let _articleDescription = document.createElement('p');
    let _articleSource = document.createElement('p');
    
    _articleItem.classList.add('article--item');
    _articleTitle.classList.add('article--title');
    _articleDescription.classList.add('article--description');
    _articleSource.classList.add('article--source');

    _articleTitle.innerHTML = article.title;
    _articleDescription.innerHTML = article.description;
    _articleSource.innerHTML = article.source.name;

    _container.appendChild(_articleItem);
    _articleItem.appendChild(_articleTitle);
    _articleItem.appendChild(_articleDescription);
    _articleItem.appendChild(_articleSource);
  }

  startDay();
  startTime();
  makeForcastCall();
  makeWeatherCall();
  getNews();
  
}());