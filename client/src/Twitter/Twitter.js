import React, { Component } from 'react';
import axios from "axios";
import TinySlider from "tiny-slider-react";
import './Twitter.css';

const settings = {
  axis: 'vertical',
  loop: true,
  autoplay: true,
  nav: false,
  controls: false,
  speed: 800,
  animateDelay: 1000,
  freezable: false,
  autoplayButton: false,
  autoplayHoverPause: false,
  container: ".twitter"
};

class Twitter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tweets: []
    }    
  }
  
  componentWillMount() {
    this.makeTwitterCall();
  }

  componentDidMount(){
    setInterval((this.makeTwitterCall), 1000 * 60 * 60);
  }

  makeTwitterCall() {
    const url = `http://localhost:3159/tweets`;

    axios.get(url)
      .then((response) => {
        this.setState({
          tweets: response.data
        })
      })
      .catch((error) => {
        console.log(error);
      });      
  }

  render() {   
    return (
      <div className="twitter">
        <TinySlider settings={settings}>
          {this.state.tweets.map((item,i) =>
            <div key={item.id} className="tweet">
              <div className="user-info">
                <img src={item.user.profile_image_url} alt=""/>
                <h3>{item.user.screen_name} <br/>
                <span>@{item.user.name}</span></h3>
              </div>
              <p className="tweet-content">{item.text}</p>
              <p className="tweet-meta">
              {item.created_at.substring(11,16)}&nbsp;-&nbsp;
              {item.created_at.substring(8,10)} {item.created_at.substring(4,8)} {item.created_at.substring(26)}
              </p>
            </div>
          )}
        </TinySlider>
      </div>
    );
  }
}

export default Twitter;

// INSTAGRAM ACCESS TOKEN
// 21973075.1677ed0.b134a2770bb34f22ab7ed7fe82710f20