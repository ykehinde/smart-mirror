import React, { Component } from 'react';
import Date from './Date/Date';
import Weather from './Weather/Weather';
import News from "./News/News";
import Twitter from './Twitter/Twitter';
import Instagram from './Instagram/Instagram';
import Messages from './Message/Message';
import './App.css';
import './font-gotham.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="upper-half container--large">
          <div className="container--small">
            <Date></Date>
            <Weather></Weather>
          </div>
          <div className="container--small">
          </div>
          <div className="container--small">
            <Messages></Messages>
          </div>
        </div>
        <div className="middle-third">
            <Instagram></Instagram>
        </div>
        <div className="lower-half container--large">
            <div className="container--small">
              <News></News>
            </div>
            <div className="container--small">
              <Twitter></Twitter>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
