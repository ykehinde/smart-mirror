const axios = require('axios');

module.exports = {

  makeWeatherCall: function (callback){
    let city = 'London';
    let url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${parameters.OW_APIKEY}`
  
    axios.get(url)
      .then(function (response) {
        return callback(null, response);
      })
      .catch(function (error) {
        return callback(error, null);
      });
  }
}
