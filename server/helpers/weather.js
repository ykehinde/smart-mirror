let parameters = require( "../config/parameters");
let axios = require( "axios");


module.exports = {
  makeWeatherCall () {
    console.log('weather call');
    
      let city = 'London';
      let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${parameters.OW_APIKEY}`

      axios.get(url)
        .then(function (data) {
          data = data.data;
          return data;
        }) 
        .catch(function (error) {
          console.log(error);
        });
  }
}